#!/bin/bash

# Create SYMLINKS of dotfiles
# - from this git repo
# - to $HOME

# ln arguments
# -f  force link even if destination file exists already
# -s  make symbolic link (link doesnt update if file moved)
# -b  make backup of pre-existing destination file


# link dotfiles
ln -fsb "$PWD/bashrc"                 "$HOME/.bashrc"
ln -fsb "$PWD/bash_aliases"           "$HOME/.bash_aliases"
ln -fsb "$PWD/dircolors"              "$HOME/.dircolors"
ln -fsb "$PWD/gitconfig"              "$HOME/.gitconfig"
ln -fsb "$PWD/inputrc"                "$HOME/.inputrc"
ln -fsb "$PWD/vimrc"                  "$HOME/.vimrc"


# remove exiting directory link
rm -Rf "$HOME/handy_scripts"
rm -Rf "$HOME/.vim"


# re-create directory links
ln -s "$PWD/handy_scripts"       "$HOME/handy_scripts"
ln -s "$PWD/vim"                 "$HOME/.vim"


# show operating system info
. get_os.sh
get_os_profile


# activate the bashrc file
source ~/.bashrc


# list all links
ls -AF -o --color $HOME | grep ^l


# give user final msg
echo
echo Dotfiles have been linked.
echo

