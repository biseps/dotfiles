#!/bin/bash

# wrapper for unix find command

if [ "$#" -lt 1 -o "$#" -gt 2 ];
then
    echo "Usage:"
    echo "\$1 - partial filename you're searching for "
    echo
    echo "\$2 - (optional)"
    echo "      Change default behaviour:" 
    echo "      -l  list file attributes (for found files)"
    echo "      -c  count number of lines in each file"
    echo "      -o  only search current directory " 
    echo "          (ie descend to a max depth of 1)"
    exit 1
fi

# $1 - partial filename you're searching for


FILENAME="$1"
behaviour="$2"

star="*"
search_term="$star$FILENAME$star"


# tip:
# find {files/directories to ignore} -prune -o {files we want} -print

# Note: ignore git and svn directories

if [ "$behaviour" == "-l" ]
then
    # print file attributes (using "ls")
    find . -type d \( -name '.git' -o -name '.svn' \) -prune -or -iwholename "$search_term" -print0 | xargs -0 ls -lh 

elif [ "$behaviour" == "-c" ]
then
    # count the number of lines in each file
    find . -type d \( -name '.git' -o -name '.svn' \) -prune -or -iwholename "$search_term" -print0 | xargs -0 wc -l 

elif [ "$behaviour" == "-o" ]
then
    # descend to a max depth of 1
    find . -maxdepth 1 -type d \( -name '.git' -o -name '.svn' \) -prune -or -iwholename "$search_term" -print | grep -i "$FILENAME" 

else
    # search sub-directories and print file names
    find . -type d \( -name '.git' -o -name '.svn' \) -prune -or -iwholename "$search_term" -print | grep -i "$FILENAME"
fi


