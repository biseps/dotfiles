#!/bin/bash

# E Farrell Easter 2013
# Find all files matching a specified pattern
# and delete them!!
#
# Arguments:
#   $1 - search term


# create a flexible search string
star="*"
search_term="$star$1$star"


# first backup files (just in case!)
printf "\nMaking a backup of the files first...\n"
file_extract.sh "$1"  "$HOME/backup_$1"

if [[ $? -ne 0 ]] ; then 
    printf "\nBackup Failed\n"
    exit 1
fi


# Double-check with user
echo
echo "Delete these files? (y/n)?"
read answer

case $answer in

    [yY]*) 
        # continue processing
        echo  "Deleting files matching : $search_term"
        ;;

    *) 
        # anything else just abort
        echo "Aborting..."
        exit 0
        ;;

esac


# Perform delete
find . -type f -iname "$search_term" -exec rm "{}" \;
