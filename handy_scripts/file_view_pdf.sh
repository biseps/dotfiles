#!/bin/bash

# E Farrell Easter 2013
# view a pdf file by just specifying part of its name
# "evince" is a linux pdf viewer

star="*"
search_term="$star$@$star"

# Perform search and then
# call pdf viewer "evince" for each match found
find . -maxdepth 1 -iname "$search_term" -exec evince "{}" \;


