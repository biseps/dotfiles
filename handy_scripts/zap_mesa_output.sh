#!/bin/bash

# clean up output from a mesa run
rm ./photos/*
rm ./LOGS/*
rm ./png/*
rm ./make/*.o
