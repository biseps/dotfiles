#!/bin/bash

if [ -z "$1" ]
then
   echo fatal: must enter a search term
else
    search_term="$@"
    star="*"
    search_pattern="$star$search_term$star"

    # Perform search and then open image
    find . -maxdepth 1 -iname "$search_pattern" -exec eog "{}" \;
fi


