#!/bin/bash

# E Farrell Summer 2013
# Find all files matching specified 
# pattern and copy to a new directory 
#


if [ -z "$1" ]
then
    # if no input parameters 
    # just echo some helpful info
    echo 
    echo "File Extraction:"
    echo "Copy a list of files to a specified TARGET directory."
    echo "The unix 'find' utility is used to identify files matching a search string."
    echo
    echo "Input parameters:"
    echo
    echo "\$1 - (required) search string"
    echo "      no need for wildcard character"
    echo
    echo "\$2 - (required) target directory "
    echo "      must be unqiue directory name"
    echo
    echo "\$3 - (optional) -f"
    echo "      do not recreate directory structure"
    echo "      and keep resulting target directory FLAT"
    exit
fi


# create a flexible search string
# from first argument
star="*"
search_term="$star$1$star"


# Copy files where?
if [ -z "$2" ]
then
    # User didnt specify directory.
    echo You must specify a target directory! 
    echo Exiting now...
    exit 1
else
    # User supplied a directory name
    target_directory="$2"
fi


if [ -d "$target_directory" ]; then
    echo "Target Directory $target_directory exists already."
    echo "You must specify a unique directory."
    echo "Exiting now..."
    exit 1
else
    # create directory to hold files
    mkdir $target_directory
fi



echo
echo "Search string     : $search_term"
echo "Target directory  : $target_directory"
echo
echo Extracting files...
echo


if [[ "$3" == "-f" ]];
then
    # Keep $target_directory structure "FLAT"
    #find . -type f -iwholename "$search_term" -print -exec cp --backup=t "{}" "$target_directory" \; > "$target_directory"/extract_list
    find . -type f -iwholename "$search_term" -print -exec cp --backup=t "{}" "$target_directory" \; 
else
    # recreate directory structure in $target_directory
    #find . -type f -iwholename "$search_term" -print -exec cp --parents "{}" "$target_directory" \; > "$target_directory"/extract_list
    find . -type f -iwholename "$search_term" -print -exec cp --parents "{}" "$target_directory" \; 
fi


# show what we extracted
cd "$target_directory"

