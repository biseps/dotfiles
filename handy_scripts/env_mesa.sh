#!/bin/bash

# Setup Mesa Star Environment

export MESA_DIR="/padata/beta/users/pmr257/repos/mesa-r6794"
#export MESA_DIR="$HOME/mesa"

export MESASDK_ROOT="/padata/beta/users/pmr257/repos/mesasdk"
#export MESASDK_ROOT="$HOME/mesasdk"

export OMP_NUM_THREADS=8
#export OMP_NUM_THREADS=2

[[ -s $MESASDK_ROOT/bin/mesasdk_init.sh ]] && . $MESASDK_ROOT/bin/mesasdk_init.sh

