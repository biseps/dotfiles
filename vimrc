" Dont mimic vi exactly
set nocompatible


" vundle required!
filetype off


" enable filetype detection etc
filetype plugin indent on


" help vim detect latex files
let g:tex_flavor = "latex"
au BufRead,BufNewFile *.tex set wrap textwidth=78
au BufRead,BufNewFile *.tex set noautoindent


" Force Fortran syntax to use free-form source
" instead of old style fixed-width Fortran
let fortran_free_source=1
let fortran_have_tabs=1

syntax on
"au BufRead,BufNewFile *.SQL set filetype=sql
"au BufRead,BufNewFile *.f set filetype=fortran
autocmd BufRead,BufNewFile *.dek        set filetype=fortran
autocmd BufRead,BufNewFile inlist*      set filetype=fortran
autocmd BufRead,BufNewFile *.defaults   set filetype=fortran
autocmd BufRead,BufNewFile *.list       set filetype=fortran
autocmd BufRead,BufNewFile *.json       set filetype=javascript
autocmd BufRead,BufNewFile *.ipynb      set filetype=javascript
autocmd BufRead,BufNewFile *.md         set filetype=markdown


" Entering QuickFix window:
" disable 'search' highlight group.
" (ensures current quickfix line NOT highlighted)
autocmd BufReadPost quickfix highlight clear search 

" when quiting QuickFix window,
" re-enable search highlighting
autocmd FileType qf nmap <buffer> qq :call ResetSearchColor()<CR> :bd<CR>


" save whatever directory I start vim from.
" Handy to have sometimes - because autochdir jumps around alot
let startup_work_dir = $PWD


" my locations
let DOTFILES_DIR = $HOME . '/dotfiles'
let REPOS_DIR    = $HOME . '/repos'
let NOTES_DIR    = REPOS_DIR . '/notes'


set hidden
set cmdheight=2
set shortmess=atI
set t_Co=256
set encoding=utf-8
set autoindent
set nofoldenable    
" set timeoutlen=600

set nobackup
set nowritebackup
set noswapfile
set noautowrite
set noautowriteall
set nohlsearch

"automatically read saved files
set autoread  

set incsearch
set pastetoggle=<F10>
set autochdir
set nowrapscan
set novisualbell
set noerrorbells
set nojoinspaces

" dont recognise underscore
" as a word boundary
set iskeyword+=_
let g:tex_isk='48-57,a-z,A-Z,192-255,_'

" ignore escape sequences in insert mode
set noesckeys 

" create a 'margin' at the
" top and bottom of the page
set scrolloff=2

"allow cursor to roam over blank space
set virtualedit=all

" dont bother with smartindent
" cos of '#' problem below
set nosmartindent

" spelling options
set spelllang=en_gb
set spellfile=$HOME/.vim/spell/en.utf-8.add


" dont jump to linestart
" when paging up and down
set nostartofline

" make backspace work in insert mode
set backspace=indent,eol,start
:fixdel

" Try and force unix line endings always
set fileformats=unix,dos,mac


" actually switch off wrapping
set nowrap
set sidescroll=5
set sidescrolloff=5


set tabstop=4
" set softtabstop=2
set softtabstop=4
set shiftwidth=4

" force all tabs to be spaces
set expandtab

" dont autowrap text
set formatoptions-=t

" Round indent to a multiple of shiftwidth
set shiftround 

" disable auto comments
autocmd FileType * setl formatoptions-=cro

" dont split words when soft-wrapping
set formatoptions+=1
set linebreak
set showbreak=...

" Show invisible characters
set listchars=tab:▸\ ,eol:¬
set listchars+=precedes:<,extends:>
set listchars+=trail:~,

" used when wrapping paragraphs with 'gq'
set textwidth=70

" how much history to save
"set viminfo='10,:10,<0,@0,f0,/10

" turn on bracket matching
set showmatch
set matchtime=2

" turn off bracket matching
" let g:loaded_matchparen=1

" Treat '<','>' as matching braces
" set matchpairs+=<:>

" use case-insensitive search
" ...unless search term has CapTials
set ignorecase
set smartcase


" use system clipboard
if has("unix")
    " X11 Clipboard register: +
    " Map '+' register to unamed register
    set clipboard=unnamedplus

elseif has("win32")
    " Windows Clipboard register: *
    " Map '*' register to unamed register
    set clipboard=unnamed
endif



" highlight line when in INSERT mode
autocmd InsertEnter * set cursorline
autocmd InsertLeave * set nocursorline



" Python- better completion and highlighting
autocmd FileType python     set omnifunc=pythoncomplete#Complete
" let python_highlight_all = 1


" 256 color
"colorscheme jellybeans

" " 256 color - for ipad
" syntax enable
" set background=dark
" colorscheme solarized


" have case insensitve completion on command line
if exists("&wildignorecase")
    set wildignorecase
endif

" Ignore various file types
" set wildmenu
set wildmode=full
set wildignore+=*.jpg,*.jpeg,*.png,*.gif
set wildignore+=*.pdf
set wildignore+=*.pyc,*.pyo
set wildignore+=*.so,*.swp,*.zip     
set wildignore+=*.swp,*.zip,*.exe  
set wildignore+=*.o,*.mod "fortran output
set wildignore+=*.fits,*.hdf5,*.h5
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*        " Linux/MacOSX
set wildignore+=*\\.git\\*,*\\.hg\\*,*\\.svn\\*  " Windows ('noshellslash')

" search for tags files
set tags=./tags,tags;

" ---------------------------------------------------------------------------
"                 Key mappings
" ---------------------------------------------------------------------------

" space is my leader key
nnoremap <SPACE> <NOP>
let mapleader = "\<SPACE>"

" unmap q, I use it for quitting
nnoremap q <NOP>




" Edit and source .vimrc file
nnoremap <leader>1      :exec 'edit '   . DOTFILES_DIR . '/vimrc'<cr>
nnoremap <leader>2      :exec 'source ' . DOTFILES_DIR . '/vimrc'<cr>

" show syntax element of current word
nnoremap <leader>3      :echo synIDattr(synID(line("."), col("."), 1), "name")<cr>

" show current color scheme
nnoremap <leader>4      :source $VIMRUNTIME/syntax/hitest.vim<cr>

" edit bashrc and bash_aliases
nnoremap <leader>8      :exec 'edit ' . DOTFILES_DIR . '/bashrc'<cr>
nnoremap <leader>9      :exec 'edit ' . DOTFILES_DIR . '/bash_aliases'<cr>






" activate spelling check
nnoremap <leader>S :setlocal spell spelllang=en_gb<CR>

" Revert to whatever directory 
" I launched Vim from...
nnoremap <leader>\ :exec 'cd ' . startup_work_dir <CR>

" always wrap full paragraph
nnoremap gq vipgq

" my special paragraph wrap
nmap gp {jo<esc>jgq{ddv}>

" select paragraph
nnoremap <leader>p vip

" select all contents
nnoremap <leader>y ggVG

" make marking easier on the fingers!
nnoremap ' `

" vim diff - next/prev diff
nnoremap \j ]c
nnoremap \k [c

" I prefer paste-left
" as default for p
nnoremap p P
nnoremap P p

" Retrieve and paste last yanked text
nnoremap Y "0P
vnoremap Y "0P

" split line in two
nnoremap <leader><enter> i<cr><Esc>


" toggle search match highlights
nmap  <F6>      :set hlsearch!<CR>


" add blank space to left/right
nnoremap  <leader>h i<Space><Esc>l
nnoremap  <leader>l a<Space><Esc>h
nmap  <leader><leader> <leader>h<leader>l


" global replace
" (.,$) means start from current line to eof
vnoremap <C-h> "hy:.,$s/<C-r>h//gc<left><left><left>
nmap <C-h> :.,$s//gc<left><left><left>



" Split windows shortcuts
nmap <leader>s :wincmd v <bar> bn <bar> wincmd w <CR>
nmap <leader>v :wincmd s <bar> bn <bar> wincmd w <CR>



" quotes/brackets for visually selected text
vnoremap <leader>d di"<C-r>""<Esc>
vnoremap <leader>s di'<C-r>"'<Esc>
vnoremap <Leader>b di(<C-r>")<Esc>
vnoremap <Leader>a di[<C-r>"]<Esc>
vnoremap <Leader>u di{<C-r>"}<Esc>
vnoremap <Leader>m di$<C-r>"$<Esc>


" delete current file and buffer
nmap <leader>X :call delete(expand('%')) <bar>  bdelete! <CR>

" navigate windows
nnoremap ; <C-W><C-W>

" alter search direction
noremap , N

" Navigate jumplist
nnoremap <C-u> <C-o>
nnoremap <C-f> <C-i>
" nnoremap <leader>r :jumps<CR>

" Cycle Buffers
nnoremap <C-n> :bn<CR>
nnoremap <C-i> :bp<CR>

" access command line
nnoremap <C-l> :

" command line (ex) mappings
cnoremap <C-o> <C-f>
cnoremap <C-t> <HOME>
cnoremap <C-e> <END>
cnoremap <C-k> <UP>
cnoremap <C-n> <DOWN>
" cnoremap <C-b> <S-Left>
" cnoremap <C-f> <S-Right>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <C-d> <Del>
cnoremap <C-g> <C-c>


" escape insert/visual mode quickly
inoremap kj <esc>
vnoremap kj <esc>


" quit buffer in window split 
" (but DONT delete window)
" warning: quit even if buffer dirty
noremap qs :bp<bar>sp<bar>bn<bar>bd!<CR>

" quit buffer ignoring changes 
" (if in split, window will be deleted too)
noremap qq :bd!<CR>

" quit all buffers ignoring changes
noremap qa :qa!<CR>

" quit window
noremap qw :q<CR>


" start blank buffer
nnoremap <c-e> :enew<CR>

" save
nnoremap s :update<CR>

" save:
" using first line in buffer as filename
nnoremap S gg^vg_"hy :Sav <C-r>h.txt

" save as (in current directory)
nnoremap <leader>a :Sav 


" insert blank lines above/below
nnoremap <leader>j o<Esc>
nnoremap <leader>k O<Esc>


"smoother page-up and page-down
vnoremap <C-j> <C-d>
nnoremap <C-j> <C-d>
nnoremap <C-k> <C-u>
vnoremap <C-k> <C-u>


"search for current word (down/up)
nnoremap gd *
nnoremap gu #


" navigate smoothly on wrapped lines
nnoremap j gj
nnoremap k gk


" go to first and last character of line
vnoremap gl g_
nnoremap gl g_
nnoremap gh ^
vnoremap gh ^

" go to end of file
vnoremap gn G
nnoremap gn G

" go to page top/middle/bottom
vnoremap gk H
nnoremap gk H
vnoremap gm M
nnoremap gm M
vnoremap gj L
nnoremap gj L

" go to paragraph start/end
vnoremap L }
nnoremap L }
vnoremap H {
nnoremap H {

" Jump to tag
nnoremap <C-]> g<C-]>

" move right quickly in insert mode
" by pressing alt+l
" (note: dont map anything else to <esc>l)
inoremap <esc>l <C-o>l
                    
" Duplicate current line
nmap <leader>d yyp

" join lines
nnoremap ga J


" text 'bubble' up and down
noremap <s-k> ddkP
noremap <s-j> ddp

" toggle hidden whitespace non-printing characters
nmap <leader>w :set list! <bar> :set cursorline!<CR>

" show line numbers in gutter margin
nmap <leader>g :set number!<CR>

" show path/name of current file
nnoremap gs 1<C-g>






" ---------------------------------------------------------------------------
"                       netrw file explorer
" ---------------------------------------------------------------------------


" open netrw in new tab
nnoremap ge :Texplore .<CR>

" Refresh directory listing
nmap gr <Plug>NetrwRefresh
                        
let g:netrw_dirhistmax=0
let g:netrw_preview = 1
let g:netrw_winsize=30
let g:netrw_fastbrowse=0        
let g:netrw_liststyle = 1

"use previous buffer when opening new file
" let g:netrw_browse_split = 4

" use current buffer when opening new file
let g:netrw_browse_split = 0

"vertical splitting right
" let g:netrw_altv = 1
let g:netrw_altv = 0











" ---------------------------------------------------------------------------
"                       Ruler / Status Line / Tab line 
" ---------------------------------------------------------------------------


set rulerformat =%1*\↓%l\ %*       "percentage (User1 color)
set rulerformat +=%<               "truncate here if ruler too long
set rulerformat +=↕%L\             "total lines
set rulerformat +=→%v              "virtual column number

set statusline=
set statusline +=%1*\ %t\ %*        "filename (User1 color)
set statusline +=%=%*               "switch to rhs of status line
set statusline +=\ %P\              "percentage 
set statusline +=%l\/               "current line
set statusline +=%L\                "total lines
set statusline +=→%v\               "virtual column number


" format tabline
" %-0{minwid}.{maxwid}{item}
set tabline=


" filename (User2 color)
set tabline +=%2*\ %t\ %* 

" truncate here if line gets too long
" (always want filename visible)
set tabline +=%<

"directory for current file
set tabline +=%\ \ \ %{expand('%:p:h')}\ 

"switch to RHS of status line
set tabline +=%=

"modified flag
set tabline +=\ %m

"read only flag
set tabline +=\ %r

"file format: unix, dos, mac
set tabline +=\ %{&ff}%*  

"file type
set tabline +=\ %y\ 


















" 0 never show
" 1 show if split window
" 2 always show
set showtabline=2
set laststatus=1
set ruler



" ---------------------- Misc FUNCTIONS -------------------------------------------------------

function! RenameFile()
    let old_name = expand('%')
    let new_name = input('New file name: ', expand('%'), 'file')

    if new_name != '' && new_name != old_name
        " escape any spaces in file name
        let format_new_name = substitute(new_name, ' ', '\\ ', 'g')
        let format_old_name = substitute(old_name, ' ', '\\ ', 'g')

        " save new file,
        exec ":saveas "  . format_new_name

        " delete old file 
        " and remove its buffer
        exec ':silent !rm ' . format_old_name
        exec ':bdelete! '   . format_old_name

        redraw!
    endif
endfunction

nmap <leader>r :call RenameFile()<cr>



" http://vim.1045645.n5.nabble.com/How-to-auto-escape-space-in-filename-when-write-td1149064.html

command! -bang -nargs=* Sav :call SaveasEscapeSpaces(<q-bang>, <q-args>)

function! SaveasEscapeSpaces(bang, filename)
        :exec "saveas" . a:bang . " " . substitute(a:filename, ' ', '\\ ', 'g')
endfu 


function! ResetSearchColor()
        :exec "hi Search         ctermfg=46   ctermbg=NONE"
        :exec "match none"
endfu 



