# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples


# If not running interactively,
# don't do anything
[ ! -t 0 ] && return
# [ -z "$PS1" ] && return


# Color constants for prompt
RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\033[0;32m\]"
BLUE="\[\033[0;34m\]"
CYAN="\[\033[0;36m\]"
LIGHT_RED="\[\033[1;31m\]"
LIGHT_GREEN="\[\033[1;32m\]"
WHITE="\[\033[1;37m\]"
LIGHT_GRAY="\[\033[0;37m\]"
DARK_GRAY="\[\033[1;30m\]"
NO_COLOR="\[\e[0m\]"
PURPLE="\[\033[0;35m\]"
LIGHT_PURPLE="\[\033[1;35m\]"

# Make sure we're using 256 colors
export TERM=screen-256color
#export TERM=xterm-256color

# control color output for GREP
export GREP_OPTIONS='--color=auto'

# Stop the terminal from freezing with ctrl-s
stty -ixon -ixoff

# don't put duplicate lines in the history.
# See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=5000
HISTFILESIZE=5000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in
# (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi





# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac


# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi


# utility to quickly change directory
if [ -f ~/handy_scripts/bashmarks.sh ]; then
    . ~/handy_scripts/bashmarks.sh
fi


# GIT autocomplete
if [ -f ~/handy_scripts/git-completion.bash ]; then
    source ~/handy_scripts/git-completion.bash

    # autocomplete for alias 'g' as well
    complete -o default -o nospace -F _git g
fi




# Get some weird CURL errors for bitbucket.
# This fixes it. (Cant be bothered to find 
# out whats the problem) 
export SSL_CERT_FILE="$HOME/.cacert.pem"


# add my directory with
# custom scripts and files
export PATH="$HOME/handy_scripts:$PATH"



#  matthewmccullough / gist:48058
function parse_git_branch {
  # git rev-parse --git-dir &> /dev/null
  # git_status="$(git status --untracked-files=no 2> /dev/null)"
  git_status="$(git status 2> /dev/null)"

  branch_pattern="^# On branch ([^${IFS}]*)"
  remote_pattern="# Your branch is (.*) of"
  diverge_pattern="# Your branch and (.*) have diverged"

  #if [[ ! ${git_status}} =~ "working directory clean" ]]; then
  if [[ ! ${git_status}} =~ "nothing to commit" ]]; then
    state="x"
  fi

  # add an else if or two here if you want to get more specific
  if [[ ${git_status} =~ ${remote_pattern} ]]; then

    if [[ ${BASH_REMATCH[1]} == "ahead" ]]; then
      remote="↑"
    else
      remote="↓"
    fi

  fi

  if [[ ${git_status} =~ ${diverge_pattern} ]]; then
    remote="↕"
  fi

  if [[ ${git_status} =~ ${branch_pattern} ]]; then
    branch=${BASH_REMATCH[1]}
    echo "${PURPLE}(${branch})${RED}${state}${YELLOW}${remote}"
  fi
}

# different prompt if SSH connection
if [ -n "$SSH_CLIENT" ]
then
    custom_host="\h"
    custom_prompt=" \$${NO_COLOR} "
else
    custom_host="\h"
    custom_prompt=" >${NO_COLOR} "
fi



# bash prompt: limit number of directories PS1 "\W" displays
export PROMPT_DIRTRIM=4


# bash prompt: show GIT info
function set_ps1_git() {
    PS1="\n${YELLOW}\w $(parse_git_branch)\n${GREEN}\u@${custom_host}${custom_prompt}"
}


# bash prompt: no GIT info (quicker)
function set_ps1_simple() {
    PS1="\n${YELLOW}\w\n${GREEN}\u@\h${custom_prompt}"
}


# Default to which bash prompt?
PROMPT_COMMAND=set_ps1_git
#PROMPT_COMMAND=set_ps1_simple



function set_ou_proxy() {

    export HTTP_PROXY='http://wwwcache.open.ac.uk:80'
    export http_proxy=$HTTP_PROXY

    export HTTPS_PROXY='https://wwwcache.open.ac.uk:80'
    export https_proxy=$HTTPS_PROXY
}




if [ -f ~/dotfiles/get_os.sh ]; then

    # perform customisations based
    # on which operating system is booting

    . ~/dotfiles/get_os.sh
    get_os_profile


    case "$OS" in

        linux)
            case "$DIST" in


                CentOS)
                    if [ -d "/padata/beta/users" ]; then
                        # Its one of the open uni machines!


                        # anaconda path
                        export PATH="/padata/beta/users/pmr257/anaconda/bin:$PATH"

                        # HTTP environment variables
                        set_ou_proxy
                    fi
                    ;;

            esac
            ;;



        *)
            ;;

    esac

fi


# Finally!
# Add CURRENT DIRECTORY to path.
# Automatically run scripts in current directory prefixing "./"
export PATH=".:$PATH"
