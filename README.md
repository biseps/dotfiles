# dotfiles #


### What is this repository for? ###

* A set of scripts to configure your UNIX environment


### How do I get set up? ###

* run **./setup.sh** script which will create a .bashrc, .bash_aliases etc
* also includes handy utilities like sqlitestudio, tig, unzip all compiled for CENTOS