#!/bin/bash

# enable color support
if [ -x /usr/bin/dircolors ]; then
    # activate my own custom ~/.dircolors
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

    # always colourise ls output
    alias ls='ls --color=auto'
fi



# cluster commands
alias qs='qstat'
alias qa='qstat -u "*" | less'


# cluster commands
function qj() { 
    job_id="$1" 
    qstat -j $job_id | less 
} 



# show log file
alias lg='tail log.txt'


# create fortran tags
alias ftags='ctags -R --exclude=".git" --fortran-kinds=+b+c+e+f+i+k+l+L+m+n+p+s+t+v'


# diff two directories
alias diffd='diff -r --exclude=".git" --exclude=".svn"'
alias diffdb='diff --brief -r --exclude=".git" --exclude=".svn"'

# add/refresh bash history
alias ha='history -a'
alias hn='history -n'

# count number of lines
alias cn='wc -l'

# list octal permissions on a file
alias ocl='stat -c "%a %n"'

# set full octal permissions
alias ocx='chmod 777'


# change bash prompt
alias setps='export PROMPT_COMMAND=set_ps1_simple'
alias setpg='export PROMPT_COMMAND=set_ps1_git'

# copy command:
# -v = verbose mode
# -i = ask before overwriting
alias cp='cp -vi'



# update stale $DISPLAY env variable
# when reattaching TMUX sessions
function upd()
{
    new_display=$(tmux show-env | sed -n 1p | sed 's/^.*://')

    export DISPLAY="localhost:$new_display"

    echo "My environment:"
    env | grep -i "DISPLAY"
    echo
    echo "Tmux environment:"
    tmux show-env
}



# create "nuke" alias to destroy files.
# Dangerous command!
# -r  recursive
# -f  force. ignore non-existent files. never prompt
# -I  asks for confirmation first
#alias nk='rm -rfI'



# ls aliases
# -A  Almost all (dont include . and ..)
# -l  long listing format
# -L  dereference symlinks. ie use target file info
# -F  Classify (append /@ etc to entries)
# -h  human readable file sizes

# list (almost) all files
alias la='ls -AFh --group-directories-first'

# list (almost) all files - long form
alias ll='ls -AlFh'


# reverse time order
alias lt='ls -lhtr | tail -n 5'

# directories only
alias ld='ls -d -w 40 */'
alias ldh='ls -dA -w 40 .*/'
alias lda='echo ; ld ; echo ; ldh'
alias ldl='LC_COLLATE=C ls -lhLAF --color | grep /$'

# list hidden files in cwd
# -L  dereference symlinks. ie use target file info
alias lh='ls -dLF --group-directories-first .*'
alias lhl='ls -dlL --group-directories-first .*'

# List symlinks
alias lk='ls -AF -o --color | grep ^l'

# delete symlinks
# -f  force deletion
alias lkdel='find . -maxdepth 1 -type l | xargs rm -f'

# list sorted DIRECTORY sizes
# -h  human readable form
alias big="du -h --max-depth 1 | sort -h"

# list sorted FILE sizes
alias bigf="du -h -a --max-depth 1 | sort -h"


# make sudo recognise aliases (trick)
alias sudo='sudo '


# ssh X-Windows
# -c = cipher, arcfour is the fastest
# -C = use compression
# -X = use X11 forwarding
alias xssh='ssh -c arcfour,blowfish-cbc -XC'


# attach to windows desktop
alias winou='rdesktop -g 80% paterminal'

# ssh Minor Debug info
alias vssh='ssh -v'

# ssh Max Debug info
alias vvssh='ssh -vvv -o LogLevel=DEBUG3'

# fix "known_hosts" error
alias fssh='ssh_fix.sh'


# Source bash config again
alias redo_bashrc='source ~/.bashrc'

# read pdfs
alias e="evince"


# number of  files (DONT incl. hidden)
alias nf='find . -maxdepth 1 -type f | grep -v "/\." | wc -l'

# number of  files (incl. hidden)
alias nfh='find . -maxdepth 1 -type f | wc -l'

# number of directories
alias nd='ls -dA */ | wc -l'
alias ndh='ls -dA .*/ | wc -l'



# bulk maniuplation of files
alias ff=file_find.sh
function ffl () { file_find.sh "$1" -l; }
function ffc () { file_find.sh "$1" -c; }
function ffo () { file_find.sh "$1" -o; }

alias fe=file_extract.sh
alias fd=file_delete.sh


# gthumb image viewer
alias gt='gthumb'
alias gta='gthumb .'

alias v='vim'

# search output from various commands
alias se='env | grep -i'
alias sa='alias | grep -i'
alias sp='ps -ef | grep -i'
alias sb='cat ~/.bitbucketrepos | grep -i'
alias sj='jl | grep -i'

# Grep aliases
# -r	recursive directory search
# -i	ignore case
# -I	ignore binary files
# -w	match whole word
# -F	treat input as fixed string 
#       (no need to escape characters like ^ or \)
function gw () {  grep -riI  --exclude-dir={.svn,.git} ${1} . ; }
function gww () { grep -riIw --exclude-dir={.svn,.git} ${1} . ; }
function gwo () { grep -iI   --exclude-dir={.svn,.git} ${1} * ; }
# function gwf () { grep -riI  --include="${2:-*}" --exclude-dir={.svn,.git} ${1} . ; }
function gwf () { grep -riI  --exclude-dir={.svn,.git} ${1} ${2} ; }


# search bash history for a certain word
alias hist='history | egrep -i'

# Git aliases
alias g='git'


# Push and store each
# directory change onto a stack.
# Also: automatically list directory contents
cd()
{
    # Note "ls" options:
    # -L  dereference symlinks. ie use target file info
    # -t  list in time changed order

	builtin pushd "$@" > /dev/null && clear && ls -Ltr
    # ls -Lt --group-directories-first
}

# list directory stack
alias d='dirs -v'   

# back to Previous in directory stack
alias b="cd +1"
alias bb="cd +2"
alias bbb="cd +3"

# jump foward to end of directory stack
alias f="cd -0"

alias a='cd ~'      # go home
alias x="cd .."
alias xx="cd ../../"
alias xxx="cd ../../../"
alias r='cd .'      # refresh directory listing
alias c='clear'     # clear screen


# sqlite stuff
alias lite='rlwrap -a -cir -D 2 -pGreen -m sqlite3'

# tmux stuff
alias ta='tmux attach'
alias tl='tmux list-sessions'
alias te='tmux show-env'
alias tka='tmux kill-server'
alias tks='tmux kill-session -t'


# conda stuff
alias coi='conda info --all'
alias coe='conda info --envs'
alias cou='conda package --untracked'

# (i)python
alias py='python'
alias ipy='ipython'
alias pyc='python -m py_compile'
alias ipy_start='ipython notebook --no-browser --port=8888 --pylab inline'
alias ipy_tunnel='ssh -c arcfour,blowfish-cbc -N -f -L localhost:8888:localhost:8888'
alias showtun='netstat -tpln | grep ssh'
alias ipy_convert='ipython nbconvert --to python "$1"'


# Open File manager,
# in current directory
naut() { nautilus "$PWD"; }


